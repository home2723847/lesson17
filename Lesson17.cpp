﻿#include <iostream>
#include <string_view>
using namespace std;

class Vector
{
public:
    Vector() : x(-1), y(-1), z(0)
    {}

    void Show()
    {
        cout << x << " " << y << " " << z << endl;
    }
    void Length()
    {
        l = abs(sqrt(x * x + y * y + z * z));
        cout << '\n' << "Length Vector is " << l << endl; ;
    }


private:
    double x;
    double y;
    double z;
    double l;

};

int main()
{
    Vector v;
    v.Show();
    v.Length();
}